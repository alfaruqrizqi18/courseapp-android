## of Course
Aplikasi kursus online yang menyediakan bahan belajar berupa video yang dapat diputar untuk dipelajari.

#### Fitur
- [x] Register User Baru
- [x] Login
- [x] Mendaftar pada kursus yang diinginkan
- [x] Dapat menambah kursus favorit
- [x] Dapat memutar video yang terdapat di dalam kursus
- [x] Dapat menyelesaikan kursus
- [x] Mengubah data akun, bahasa dan kata sandi

#### Teknologi
- [Android Studio](https://developer.android.com/studio)
- [Java](https://java.com)

#### Download link
[of Course v1 - Google Drive](https://drive.google.com/open?id=1400DO6rkvfHja2ufpYHETiTzOb1FzhNH)

#### Screenshot

| Register | Login |
| :---: | :---: |
| <a href="./screenshot/register.jpg"> <img src="./screenshot/register.jpg" width="50%"></a> | <a href="screenshot/login.jpg"> <img src="screenshot/login.jpg" width="50%"></a>

| Home | Explore |
| :---: | :---: |
| <a href="./screenshot/home.jpg"> <img src="./screenshot/home.jpg" width="50%"></a> | <a href="screenshot/explore.jpg"> <img src="screenshot/explore.jpg" width="50%"></a>

| Detail Course Tab Description | Detail Course Tab Video |
| :---: | :---: |
| <a href="./screenshot/detail_course_tab_about.jpg"> <img src="./screenshot/detail_course_tab_about.jpg" width="50%"></a> | <a href="screenshot/detail_course_tab_video.jpg"> <img src="screenshot/detail_course_tab_video.jpg" width="50%"></a>

| Pengaturan Akun | Form Edit Akun |
| :---: | :---: |
| <a href="./screenshot/account_setting.jpg"> <img src="./screenshot/account_setting.jpg" width="50%"></a> | <a href="screenshot/account_form.jpg"> <img src="screenshot/account_form.jpg" width="50%"></a>

| Tentang Aplikasi | Form Edit Password |
| :---: | :---: |
| <a href="./screenshot/about.jpg"> <img src="./screenshot/about.jpg" width="50%"></a> | <a href="screenshot/account_form_password.jpg"> <img src="screenshot/account_form_password.jpg" width="50%"></a>
