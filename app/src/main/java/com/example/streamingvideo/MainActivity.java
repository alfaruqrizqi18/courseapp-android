package com.example.streamingvideo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.streamingvideo.authactivity.LoginActivity;
import com.example.streamingvideo.fragments.AccountFragment;
import com.example.streamingvideo.fragments.ExploreFragment;
import com.example.streamingvideo.fragments.HomeFragment;
import com.example.streamingvideo.fragments.NotificationFragment;
import com.example.streamingvideo.multilanguage_helper.Language;
import com.example.streamingvideo.sharedpreference.SharefPreferenceManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class MainActivity extends AppCompatActivity {
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (!SharefPreferenceManager.getInstance(getApplicationContext()).isLoggedIn()) {
            startActivity(new Intent(MainActivity.this, LoginActivity.class));
            finish();
        } else {
            String lang = SharefPreferenceManager.getInstance(this).showSession().get("LANGUAGE");
            new Language().setLanguangeRecreate(this, lang);
            toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
//            showToolbar("Homepage");


            getSupportActionBar().hide();
            getSupportActionBar().setTitle(null);

            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
            navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
            loadFragment(new HomeFragment());
        }
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;
            switch (item.getItemId()) {
                case R.id.nav_home:
                    getSupportActionBar().hide();
                    getSupportActionBar().setTitle(null);
                    fragment = new HomeFragment();
                    loadFragment(fragment);
                    return true;
                case R.id.nav_explore:
                    getSupportActionBar().hide();
                    getSupportActionBar().setTitle(null);
                    fragment = new ExploreFragment();
                    loadFragment(fragment);
                    return true;
//                case R.id.nav_notification:
//                    showToolbar("Notifications");
//                    fragment = new NotificationFragment();
//                    loadFragment(fragment);
//                    return true;
                case R.id.nav_account:
                    getSupportActionBar().hide();
                    getSupportActionBar().setTitle(null);
                    fragment = new AccountFragment();
                    loadFragment(fragment);
                    return true;
            }
            return false;
        }
    };

    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.commit();
    }

    private void showToolbar(String title) {
        getSupportActionBar().show();
        getSupportActionBar().setTitle(title);
    }

}
