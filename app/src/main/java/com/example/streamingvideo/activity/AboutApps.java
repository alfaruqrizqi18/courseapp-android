package com.example.streamingvideo.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.streamingvideo.R;
import com.example.streamingvideo.adapters.ListLibrariesAdapter;
import com.example.streamingvideo.models.Course;
import com.example.streamingvideo.models.ListLibraries;
import com.example.streamingvideo.multilanguage_helper.Language;
import com.example.streamingvideo.sharedpreference.SharefPreferenceManager;
import com.example.streamingvideo.url.All;
import com.example.streamingvideo.url.BaseUrl;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class AboutApps extends AppCompatActivity {
    RecyclerView rv_library;
    List<ListLibraries> listLibrariesList = new ArrayList<>();
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_apps);
        String lang = SharefPreferenceManager.getInstance(this).showSession().get("LANGUAGE");
        new Language().setLanguangeRecreate(this, lang);
        rv_library = findViewById(R.id.rv_library);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setupToolbar();
        getList();
    }
    private void setupToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(this.getResources().getString(R.string.about_apps));
    }

    private void setupRecyclerView() {
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rv_library.setLayoutManager(layoutManager);
        rv_library.setNestedScrollingEnabled(false);
    }

    private void setupAdapter() {
        ListLibrariesAdapter listLibrariesAdapter = new ListLibrariesAdapter(listLibrariesList, getApplicationContext());
        rv_library.setAdapter(listLibrariesAdapter);
    }

    private void getList() {
        AndroidNetworking.get(All.index_list_libraries)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray data = response.getJSONArray("data");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject info = data.getJSONObject(i);
                                ListLibraries listLibraries = new ListLibraries();
                                listLibraries.setId_libs(info.getString("id_libs"));
                                listLibraries.setName(info.getString("name"));
                                listLibraries.setImplement(info.getString("how_to_implement"));
                                listLibraries.setLink(info.getString("link_address"));
                                listLibrariesList.add(listLibraries);
                            }
                            setupAdapter();
                            setupRecyclerView();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
