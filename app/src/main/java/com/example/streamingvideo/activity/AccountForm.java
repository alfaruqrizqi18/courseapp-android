package com.example.streamingvideo.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.streamingvideo.MainActivity;
import com.example.streamingvideo.R;
import com.example.streamingvideo.loading.Loading;
import com.example.streamingvideo.models.User;
import com.example.streamingvideo.multilanguage_helper.Language;
import com.example.streamingvideo.sharedpreference.SharefPreferenceManager;
import com.example.streamingvideo.url.All;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

import life.sabujak.roundedbutton.RoundedButton;

public class AccountForm extends AppCompatActivity {
    RoundedButton save_general_information, save_password_changes;
    EditText first_name, last_name, email, username, confirmation_password;
    EditText old_password, new_password, confirmation_new_password;
    Toolbar toolbar;
    RadioGroup radioLang;
    RadioButton radioButtonLang;
    final Loading loading = new Loading();
    String lang = SharefPreferenceManager.getInstance(AccountForm.this).showSession().get("LANGUAGE");
    int selectedId = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_form);
        AndroidNetworking.initialize(this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        radioLang = findViewById(R.id.radioLang);
        selectedId = radioLang.getCheckedRadioButtonId();
        radioButtonLang = findViewById(selectedId);
        first_name = findViewById(R.id.first_name);
        last_name = findViewById(R.id.last_name);
        email = findViewById(R.id.email);
        username = findViewById(R.id.username);
        confirmation_password = findViewById(R.id.confirmation_password);
        old_password = findViewById(R.id.old_password);
        new_password = findViewById(R.id.new_password);
        confirmation_new_password = findViewById(R.id.confirmation_new_password);
        save_general_information = findViewById(R.id.save_general_information);
        save_password_changes = findViewById(R.id.save_password_changes);

        if (lang.equals("in")) {
            radioLang.check(R.id.radioIndonesia);
        } else {
            radioLang.check(R.id.radioEnglish);
        }

        setupToolbar();
        setButtonClick();
        setLanguageClick();

    }



    @Override
    protected void onResume() {
        super.onResume();
        setAccount();
        new Language().setLanguangeRecreate(AccountForm.this, lang);
    }

    private void setLanguageClick(){
        radioLang.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radioIndonesia:
                        saveLanguageChanges("in");
                        break;
                    case R.id.radioEnglish:
                        saveLanguageChanges("en");
                        break;
                }
            }
        });
    }
    private void setupToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setToolbarTitle();
    }

    private void setToolbarTitle(){
        if (lang.equals("in")) {
            getSupportActionBar().setTitle("Akun "+sharedPref("FIRST_NAME"));
        } else {
            getSupportActionBar().setTitle(sharedPref("FIRST_NAME") + "'s account");
        }
    }

    private String sharedPref(String key){
        String shared_pref = SharefPreferenceManager.getInstance(this).showSession().get(key);
        return shared_pref;
    }

    private void setNullPassword(){
        confirmation_password.setText(null);
        new_password.setText(null);
        old_password.setText(null);
        confirmation_new_password.setText(null);
    }

    private void setAccount(){
        first_name.clearFocus();
        first_name.setText(sharedPref("FIRST_NAME"));
        last_name.setText(sharedPref("LAST_NAME"));
        email.setText(sharedPref("EMAIL"));
        username.setText(sharedPref("USERNAME"));
        setToolbarTitle();
    }

    private void setButtonClick(){
        save_general_information.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
                if (first_name.getText().toString().isEmpty()
                        || last_name.getText().toString().isEmpty()
                        || email.getText().toString().isEmpty()
                        || username.getText().toString().isEmpty()
                        || confirmation_password.getText().toString().isEmpty()) {
                    loading.alertDialogWithPositiveButton(AccountForm.this,
                            AccountForm.this.getResources().getString(R.string.warning),
                            AccountForm.this.getResources().getString(R.string.fill_all_general_information));
                } else if (!email.getText().toString().matches(emailPattern)) {
                    loading.alertDialogWithPositiveButton(AccountForm.this,
                            AccountForm.this.getResources().getString(R.string.warning),
                            AccountForm.this.getResources().getString(R.string.use_valid_email));
                } else {
                    final AlertDialog.Builder alertDialogBuilder;
                    alertDialogBuilder = new AlertDialog.Builder(AccountForm.this);
                    alertDialogBuilder.setTitle(
                            AccountForm.this.getResources().getString(R.string.confirmation));
                    alertDialogBuilder
                            .setMessage(AccountForm.this.getResources().getString(R.string.do_you_want_to_save_general_information))
                            .setCancelable(false)
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            })
                            .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    saveGeneralInformation();
                                }
                            });
                    alertDialogBuilder.show();
                }
            }
        });

        save_password_changes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (old_password.getText().toString().isEmpty()
                        || new_password.getText().toString().isEmpty()
                        || confirmation_new_password.getText().toString().isEmpty()) {
                    loading.alertDialogWithPositiveButton(AccountForm.this,
                            AccountForm.this.getResources().getString(R.string.warning),
                            AccountForm.this.getResources().getString(R.string.fill_all_password_changes));
                } else if(!confirmation_new_password.getText().toString().equals(new_password.getText().toString())) {
                    loading.alertDialogWithPositiveButton(AccountForm.this,
                            AccountForm.this.getResources().getString(R.string.warning),
                            AccountForm.this.getResources().getString(R.string.new_password_and_confirmat_new_password_not_match));
                } else {
                    final AlertDialog.Builder alertDialogBuilder;
                    alertDialogBuilder = new AlertDialog.Builder(AccountForm.this);
                    AccountForm.this.getResources().getString(R.string.confirmation);
                    alertDialogBuilder
                            .setTitle(AccountForm.this.getResources().getString(R.string.confirmation))
                            .setMessage(AccountForm.this.getResources().getString(R.string.do_you_want_to_save_general_information))
                            .setCancelable(false)
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            })
                            .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    savePasswordChanges();
                                }
                            });
                    alertDialogBuilder.show();
                }
            }
        });
    }

    private void saveGeneralInformation(){
        String id_user = SharefPreferenceManager.getInstance(AccountForm.this).showSession().get("ID_USER");
        loading.spinnerLoading(
                AccountForm.this,
                AccountForm.this.getResources().getString(R.string.saving),
                AccountForm.this.getResources().getString(R.string.please_wait));
        AndroidNetworking.post(All.save_general_information)
                .addBodyParameter("id_user", id_user)
                .addBodyParameter("first_name", first_name.getText().toString().trim())
                .addBodyParameter("last_name", last_name.getText().toString().trim())
                .addBodyParameter("username", username.getText().toString().trim())
                .addBodyParameter("email", email.getText().toString().trim())
                .addBodyParameter("confirmation_password", confirmation_password.getText().toString().trim())
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String message = response.getString("message");
                            String unique_message = response.getString("unique_message");
                            if (message.equals("confirmation_password_success")) {

                                if (unique_message.equals("username_cannot_use")) {
                                    loading.dismissLoading();
                                    loading.alertDialogWithPositiveButton(AccountForm.this,
                                            "Warning",
                                            AccountForm.this.getResources().getString(R.string.username_duplicate));
                                } else if (unique_message.equals("username_can_use_and_email_cannot_use")) {
                                    loading.dismissLoading();
                                    loading.alertDialogWithPositiveButton(AccountForm.this,
                                            "Warning",
                                            AccountForm.this.getResources().getString(R.string.email_duplicate));
                                } else if (unique_message.equals("username_can_use_and_email_can_use")) {
                                    loading.dismissLoading();
                                    JSONObject data = response.getJSONObject("data");
                                    User user = new User(
                                            data.getString("id_user"),
                                            data.getString("first_name"),
                                            data.getString("last_name"),
                                            data.getString("email"),
                                            data.getString("username"),
                                            data.getString("language"),
                                            data.getString("created_at")
                                    );
                                    SharefPreferenceManager.getInstance(getApplicationContext()).onLogin(user);

                                    final AlertDialog.Builder alertDialogBuilder;
                                    alertDialogBuilder = new AlertDialog.Builder(AccountForm.this);
                                    alertDialogBuilder.setTitle(AccountForm.this.getResources().getString(R.string.congratulations));
                                    alertDialogBuilder
                                            .setMessage(AccountForm.this.getResources().getString(R.string.save_general_information_success))
                                            .setCancelable(false)
                                            .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog,int id) {
                                                    dialog.dismiss();
                                                    setAccount();
                                                    setToolbarTitle();
                                                    setNullPassword();
                                                }
                                            });
                                    alertDialogBuilder.show();
                                }

                            } else {
                                loading.dismissLoading();
                                loading.alertDialogWithPositiveButton(AccountForm.this,
                                        "Warning",
                                        AccountForm.this.getResources().getString(R.string.confirmation_password_general_information_failed));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });
    }

    private void savePasswordChanges(){
        String id_user = SharefPreferenceManager.getInstance(AccountForm.this).showSession().get("ID_USER");
        loading.spinnerLoading(
                AccountForm.this,
                AccountForm.this.getResources().getString(R.string.saving),
                AccountForm.this.getResources().getString(R.string.please_wait));
        AndroidNetworking.post(All.save_password_changes)
                .addBodyParameter("id_user", id_user)
                .addBodyParameter("old_password", old_password.getText().toString().trim())
                .addBodyParameter("new_password", new_password.getText().toString().trim())
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String message = response.getString("message");
                            if (message.equals("checking_password_success")) {
                                    loading.dismissLoading();
                                    final AlertDialog.Builder alertDialogBuilder;
                                    alertDialogBuilder = new AlertDialog.Builder(AccountForm.this);
                                    alertDialogBuilder.setTitle(
                                            AccountForm.this.getResources().getString(R.string.congratulations));
                                    alertDialogBuilder
                                            .setMessage(
                                                    AccountForm.this.getResources().getString(
                                                            R.string.save_password_changes_success))
                                            .setCancelable(false)
                                            .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog,int id) {
                                                    dialog.dismiss();
                                                    setNullPassword();
                                                }
                                            });
                                    alertDialogBuilder.show();

                            } else {
                                loading.dismissLoading();
                                loading.alertDialogWithPositiveButton(AccountForm.this,
                                        AccountForm.this.getResources().getString(R.string.warning),
                                        AccountForm.this.getResources().getString(R.string.old_password_incorrect));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });
    }

    private void saveLanguageChanges(String lang){
        String id_user = SharefPreferenceManager.getInstance(AccountForm.this).showSession().get("ID_USER");
        loading.spinnerLoading(
                AccountForm.this,
                AccountForm.this.getResources().getString(R.string.saving),
                AccountForm.this.getResources().getString(R.string.please_wait));
        AndroidNetworking.post(All.save_language_changes)
                .addBodyParameter("id_user", id_user)
                .addBodyParameter("language", lang)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String message = response.getString("message");
                            if (message.equals("success")) {
                                loading.dismissLoading();
                                JSONObject data = response.getJSONObject("data");
                                User user = new User(
                                        data.getString("id_user"),
                                        data.getString("first_name"),
                                        data.getString("last_name"),
                                        data.getString("email"),
                                        data.getString("username"),
                                        data.getString("language"),
                                        data.getString("created_at")
                                );
                                SharefPreferenceManager.getInstance(getApplicationContext()).onLogin(user);
                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
