package com.example.streamingvideo.activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.streamingvideo.R;
import com.example.streamingvideo.adapters.AllFavoriteCourseAdapter;
import com.example.streamingvideo.models.Course;
import com.example.streamingvideo.multilanguage_helper.Language;
import com.example.streamingvideo.sharedpreference.SharefPreferenceManager;
import com.example.streamingvideo.url.All;
import com.example.streamingvideo.url.BaseUrl;
import com.facebook.shimmer.ShimmerFrameLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class AllFavoriteCourses extends AppCompatActivity {
    Toolbar toolbar;
    EditText search_course;
    TextView search_not_found_text;
    RecyclerView rv_course;
    ShimmerFrameLayout mShimmerViewContainer;
    CardView reset_button;
    View search_not_found_layout, has_no_favorite_layout;
    List<Course> favoriteCourseList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_favorite_course);
        String lang = SharefPreferenceManager.getInstance(this).showSession().get("LANGUAGE");
        new Language().setLanguangeRecreate(this, lang);

        toolbar                 = (Toolbar) findViewById(R.id.toolbar);
        mShimmerViewContainer   = findViewById(R.id.shimmer_view_container);
        search_course           = findViewById(R.id.search_course);
        reset_button            = findViewById(R.id.reset_button);
        rv_course               = findViewById(R.id.rv_course);
        has_no_favorite_layout  = findViewById(R.id.has_no_favorite_layout);
        search_not_found_layout = findViewById(R.id.search_not_found_layout);
        search_not_found_text   = search_not_found_layout.findViewById(R.id.search_not_found_text);
        starterPack();
    }


    private void starterPack() {
        setupToolbar();
        setupRecyclerView();
        setupResetButton();
        hideSearchNotFound();
        queryOnSearch();
    }

    private void setupResetButton(){
        reset_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeKeyboard();
                getAllFavoriteCourse();
                search_course.setText(null);
                search_course.clearFocus();
            }
        });
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(this.getResources().getString(R.string.favorite));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void setupRecyclerView() {
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rv_course.setLayoutManager(layoutManager);
        rv_course.setNestedScrollingEnabled(false);
    }

    private void stopShimmer(){
        mShimmerViewContainer.stopShimmer();
        mShimmerViewContainer.setVisibility(View.GONE);
    }

    private void startShimmer(){
        mShimmerViewContainer.startShimmer();
        mShimmerViewContainer.setVisibility(View.VISIBLE);
    }

    private void setupAdapter(){
        AllFavoriteCourseAdapter allFavoriteCourseAdapter = new AllFavoriteCourseAdapter(favoriteCourseList, getApplicationContext());
        rv_course.setAdapter(allFavoriteCourseAdapter);
    }

    private void showSearchNotFound(String query) {
        search_not_found_layout.setVisibility(View.VISIBLE);
        search_not_found_text.setText(getString(R.string.search_not_found) + " " + "\""+ query+ "\"");
    }

    private void hideSearchNotFound() {
        search_not_found_layout.setVisibility(View.GONE);
    }

    private void hideHasNoFavorite() {
        has_no_favorite_layout.setVisibility(View.GONE);
    }

    private void showHasNoFavorite() {
        has_no_favorite_layout.setVisibility(View.VISIBLE);
    }

    private void queryOnSearch(){
        search_course.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    searchCourseByQuery();
                    closeKeyboard();
                    search_course.clearFocus();
                    return true;
                }
                return false;
            }
        });
    }

    private void closeKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void getAllFavoriteCourse(){
        String id_user = SharefPreferenceManager.getInstance(this).showSession().get("ID_USER");
        favoriteCourseList.clear();
        startShimmer();
        AndroidNetworking.get(All.index_favorite_course + id_user )
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            stopShimmer();
                            JSONArray data = response.getJSONArray("data");
                            String message = response.getString("message");
                            if (message.equals("has_favorite")) {
                                for (int i = 0; i < data.length(); i++) {
                                    JSONObject info = data.getJSONObject(i);
                                    JSONObject courseObject = info.getJSONObject("course");
                                    Course course = new Course();
                                    course.setId_course(courseObject.getString("id_course"));
                                    course.setTitle(courseObject.getString("title"));
                                    course.setStart_at(courseObject.getString("start_at"));
                                    course.setEnd_at(courseObject.getString("end_at"));
                                    course.setTag(courseObject.getString("tag"));
                                    course.setCategory_name(courseObject.getJSONObject("category").getString("name"));
                                    course.setColor(courseObject.getJSONObject("category").getString("color"));
                                    course.setCover_url(BaseUrl.base_url_web + courseObject.getString("cover_url"));
                                    course.setTotal_video(courseObject.getJSONArray("video").length());
                                    favoriteCourseList.add(course);
                                }
                                setupAdapter();
                                hideSearchNotFound();
                                hideHasNoFavorite();
                            } else if (message.equals("has_no_favorite")){
                                showHasNoFavorite();
                                hideSearchNotFound();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });
    }

    public void searchCourseByQuery(){
        String id_user = SharefPreferenceManager.getInstance(AllFavoriteCourses.this).showSession().get("ID_USER");
        favoriteCourseList.clear();
        startShimmer();
        final String query = search_course.getText().toString().trim();
        String url_with_query = All.index_favorite_course + id_user + "/"+ "?query=" + query;
        AndroidNetworking.get(url_with_query)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray data = response.getJSONArray("data");
                            String message = response.getString("message");
                            if (message.equals("data_found")) {
                                for (int i = 0; i < data.length(); i++) {
                                    JSONObject info = data.getJSONObject(i);
                                    JSONObject courseObject = info.getJSONObject("course");
                                    Course course = new Course();
                                    course.setId_course(courseObject.getString("id_course"));
                                    course.setTitle(courseObject.getString("title"));
                                    course.setStart_at(courseObject.getString("start_at"));
                                    course.setEnd_at(courseObject.getString("end_at"));
                                    course.setTag(courseObject.getString("tag"));
                                    course.setCategory_name(courseObject.getJSONObject("category").getString("name"));
                                    course.setColor(courseObject.getJSONObject("category").getString("color"));
                                    course.setCover_url(BaseUrl.base_url_web + courseObject.getString("cover_url"));
                                    course.setTotal_video(courseObject.getJSONArray("video").length());
                                    favoriteCourseList.add(course);
                                }
                                setupAdapter();
                                hideSearchNotFound();
                                hideHasNoFavorite();
                                stopShimmer();
                            } else if(message.equals("data_not_found")){
                                stopShimmer();
                                showSearchNotFound(query);
                                hideHasNoFavorite();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getAllFavoriteCourse();
        search_course.clearFocus();
        mShimmerViewContainer.startShimmer();
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmer();
        super.onPause();
    }
}
