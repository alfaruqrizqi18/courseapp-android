package com.example.streamingvideo.activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.streamingvideo.R;
import com.example.streamingvideo.adapters.AllLatestCourseFromViewAllAdapter;
import com.example.streamingvideo.models.Course;
import com.example.streamingvideo.multilanguage_helper.Language;
import com.example.streamingvideo.sharedpreference.SharefPreferenceManager;
import com.example.streamingvideo.url.All;
import com.example.streamingvideo.url.BaseUrl;
import com.facebook.shimmer.ShimmerFrameLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class AllLatestCourses extends AppCompatActivity {
    Toolbar toolbar;
    EditText search_course;
    TextView  search_not_found_text;
    RecyclerView rv_course;
    ShimmerFrameLayout mShimmerViewContainer;
    View search_not_found_layout;
    List<Course> courseList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_latest_courses);
        String lang = SharefPreferenceManager.getInstance(this).showSession().get("LANGUAGE");
        new Language().setLanguangeRecreate(this, lang);

        toolbar                 = (Toolbar) findViewById(R.id.toolbar);
        mShimmerViewContainer   = findViewById(R.id.shimmer_view_container);
        search_course           = findViewById(R.id.search_course);
        rv_course               = findViewById(R.id.rv_course);
        search_not_found_layout = findViewById(R.id.search_not_found_layout);
        search_not_found_text   = search_not_found_layout.findViewById(R.id.search_not_found_text);
        starterPack();
    }

    private void starterPack() {
        search_course.clearFocus();
        getAllLatestCourse();
        setupToolbar();
        setupRecyclerView();
        hideSearchNotFound();
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(this.getResources().getString(R.string.latest_courses));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        queryOnSearch();
    }

    private void setupRecyclerView() {
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rv_course.setLayoutManager(layoutManager);
        rv_course.setNestedScrollingEnabled(false);
    }

    private void stopShimmer(){
        mShimmerViewContainer.stopShimmer();
        mShimmerViewContainer.setVisibility(View.GONE);
    }

    private void startShimmer(){
        mShimmerViewContainer.startShimmer();
        mShimmerViewContainer.setVisibility(View.VISIBLE);
    }

    private void setupAdapter(){
        AllLatestCourseFromViewAllAdapter allLatestCourseFromViewAllAdapter = new AllLatestCourseFromViewAllAdapter(courseList, getApplicationContext());
        rv_course.setAdapter(allLatestCourseFromViewAllAdapter);
    }

    private void showSearchNotFound(String query) {
        search_not_found_layout.setVisibility(View.VISIBLE);
        search_not_found_text.setText(getString(R.string.search_not_found) + " " + "\""+ query+ "\"");
    }

    private void hideSearchNotFound() {
        search_not_found_layout.setVisibility(View.GONE);
    }

    private void queryOnSearch(){
        search_course.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    searchCourseByQuery();
                    closeKeyboard();
                    return true;
                }
                return false;
            }
        });
    }

    private void closeKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void getAllLatestCourse(){
        courseList.clear();
        startShimmer();
        AndroidNetworking.get(All.latest_course)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray data = response.getJSONArray("data");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject info = data.getJSONObject(i);
                                Course course = new Course();
                                course.setId_course(info.getString("id_course"));
                                course.setTitle(info.getString("title"));
                                course.setStart_at(info.getString("start_at"));
                                course.setEnd_at(info.getString("end_at"));
                                course.setTag(info.getString("tag"));
                                course.setCategory_name(info.getJSONObject("category").getString("name"));
                                course.setColor(info.getJSONObject("category").getString("color"));
                                course.setCover_url(BaseUrl.base_url_web + info.getString("cover_url"));
                                course.setTotal_video(info.getJSONArray("video").length());
                                courseList.add(course);
                            }
                            setupAdapter();
                            stopShimmer();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });
    }

    public void searchCourseByQuery(){
        courseList.clear();
        startShimmer();
        final String query = search_course.getText().toString().trim();
        String url_with_query = All.latest_course + "?query=" + query;
        AndroidNetworking.get(url_with_query)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray data = response.getJSONArray("data");
                            if (data.length() > 0 ) {
                                for (int i = 0; i < data.length(); i++) {
                                    JSONObject info = data.getJSONObject(i);
                                    Course course = new Course();
                                    course.setId_course(info.getString("id_course"));
                                    course.setTitle(info.getString("title"));
                                    course.setStart_at(info.getString("start_at"));
                                    course.setEnd_at(info.getString("end_at"));
                                    course.setTag(info.getString("tag"));
                                    course.setCategory_name(info.getJSONObject("category").getString("name"));
                                    course.setColor(info.getJSONObject("category").getString("color"));
                                    course.setCover_url(BaseUrl.base_url_web + info.getString("cover_url"));
                                    course.setTotal_video(info.getJSONArray("video").length());
                                    courseList.add(course);
                                }
                                setupAdapter();
                                hideSearchNotFound();
                                stopShimmer();
                            } else {
                                stopShimmer();
                                showSearchNotFound(query);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmer();
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmer();
        super.onPause();
    }
}
