package com.example.streamingvideo.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.streamingvideo.R;
import com.example.streamingvideo.adapters.AllRecommendedCourseFromViewAllAdapter;
import com.example.streamingvideo.models.Course;
import com.example.streamingvideo.multilanguage_helper.Language;
import com.example.streamingvideo.sharedpreference.SharefPreferenceManager;
import com.example.streamingvideo.url.All;
import com.example.streamingvideo.url.BaseUrl;
import com.facebook.shimmer.ShimmerFrameLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class AllRecommendedCourses extends AppCompatActivity {
    Toolbar toolbar;
    RecyclerView rv_course;
    ShimmerFrameLayout mShimmerViewContainer;
    LinearLayout recommendation_not_found, new_comer_meesage;
    List<Course> recommendedCourseList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_recommended_course);
        String lang = SharefPreferenceManager.getInstance(this).showSession().get("LANGUAGE");
        new Language().setLanguangeRecreate(this, lang);

        toolbar                 = (Toolbar) findViewById(R.id.toolbar);
        recommendation_not_found = findViewById(R.id.recommendation_not_found);
        new_comer_meesage       = findViewById(R.id.new_comer_meesage);
        mShimmerViewContainer   = findViewById(R.id.shimmer_view_container);
        rv_course               = findViewById(R.id.rv_course);
        starterPack();
    }

    private void starterPack() {
        recommendation_not_found.setVisibility(View.GONE);
        new_comer_meesage.setVisibility(View.GONE);
        getAllRecommendedCourse();
        setupToolbar();
        setupRecyclerView();
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(this.getResources().getString(R.string.recommended));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void setupRecyclerView() {
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rv_course.setLayoutManager(layoutManager);
        rv_course.setNestedScrollingEnabled(false);
    }

    private void stopShimmer(){
        mShimmerViewContainer.stopShimmer();
        mShimmerViewContainer.setVisibility(View.GONE);
    }

    private void startShimmer(){
        mShimmerViewContainer.startShimmer();
        mShimmerViewContainer.setVisibility(View.VISIBLE);
    }

    private void setupAdapter(){
        AllRecommendedCourseFromViewAllAdapter allRecommendedCourseFromViewAllAdapter =
                new AllRecommendedCourseFromViewAllAdapter(recommendedCourseList, getApplicationContext());
        rv_course.setAdapter(allRecommendedCourseFromViewAllAdapter);
    }


    public void getAllRecommendedCourse(){
        String id_user = SharefPreferenceManager.getInstance(getApplicationContext()).showSession().get("ID_USER");
        recommendedCourseList.clear();
        startShimmer();
        AndroidNetworking.get(All.recommended_course + id_user)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String message = response.getString("message");
                            if (message.equals("data_found")) {
                                JSONArray data = response.getJSONArray("recommended_course");
                                if (data.length() > 0) {
                                    for (int i = 0; i < data.length(); i++) {
                                        JSONObject info = data.getJSONObject(i);
                                        Course course = new Course();
                                        course.setId_course(info.getString("id_course"));
                                        course.setTitle(info.getString("title"));
                                        course.setStart_at(info.getString("start_at"));
                                        course.setEnd_at(info.getString("end_at"));
                                        course.setTag(info.getString("tag"));
                                        course.setCategory_name(info.getJSONObject("category").getString("name"));
                                        course.setColor(info.getJSONObject("category").getString("color"));
                                        course.setCover_url(BaseUrl.base_url_web + info.getString("cover_url"));
                                        course.setTotal_video(info.getJSONArray("video").length());
                                        recommendedCourseList.add(course);
                                    }
                                    rv_course.setVisibility(View.VISIBLE);
                                    setupAdapter();
                                } else {
                                    new_comer_meesage.setVisibility(View.GONE);
                                    recommendation_not_found.setVisibility(View.VISIBLE);
                                    rv_course.setVisibility(View.GONE);
                                }
                            } else {
                                rv_course.setVisibility(View.GONE);
                                new_comer_meesage.setVisibility(View.VISIBLE);
                                recommendation_not_found.setVisibility(View.GONE);
                            }
                            stopShimmer();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmer();
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmer();
        super.onPause();
    }

}
