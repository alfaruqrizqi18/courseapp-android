package com.example.streamingvideo.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.geolo.library.taggroup.GeoloTagGroup;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.streamingvideo.R;
import com.example.streamingvideo.detailactivity.CourseDetail;
import com.example.streamingvideo.models.Course;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class AllFavoriteCourseAdapter extends RecyclerView.Adapter<AllFavoriteCourseAdapter.CourseViewHolder> {

    private List<Course> courseList;
    private Context mContext;


    public AllFavoriteCourseAdapter(List<Course> courseList, Context mContext) {
        this.courseList = courseList;
        this.mContext = mContext;
    }

    public class CourseViewHolder extends RecyclerView.ViewHolder{
        ImageView cover;
        TextView title, category_name, start_at, end_at, total_video;
        CardView card_category;
        GeoloTagGroup tag;
        public CourseViewHolder(View itemView) {
            super(itemView);
            cover               = itemView.findViewById(R.id.cover);
            title               = itemView.findViewById(R.id.title);
            category_name       = itemView.findViewById(R.id.category_name);
            start_at            = itemView.findViewById(R.id.start_at);
            end_at              = itemView.findViewById(R.id.end_at);
            total_video         = itemView.findViewById(R.id.total_video);
            card_category       = itemView.findViewById(R.id.card_category);
            tag                 = itemView.findViewById(R.id.tag);
        }
    }

    @Override
    public AllFavoriteCourseAdapter.CourseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_card_all_favorite_course, parent, false);

        return new CourseViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CourseViewHolder courseViewHolder, int i) {
        final Course course = courseList.get(i);
        courseViewHolder.title.setText(course.getTitle());

        try {
            SimpleDateFormat sdfSource = new SimpleDateFormat("yyyy-mm-dd");
            Date start_at = sdfSource.parse(course.getStart_at());
            Date end_at = sdfSource.parse(course.getEnd_at());
            SimpleDateFormat sdfDestination = new SimpleDateFormat("dd MMM yyyy");
            courseViewHolder.start_at.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_date_range_black_24dp,0,0,0
            );
            courseViewHolder.start_at.setText(sdfDestination.format(start_at));
            courseViewHolder.end_at.setText(sdfDestination.format(end_at));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        courseViewHolder.card_category.setCardBackgroundColor(Color.parseColor(course.getColor()));
        courseViewHolder.category_name.setText(course.getCategory_name());
        courseViewHolder.total_video.setCompoundDrawablesWithIntrinsicBounds(
                R.drawable.ic_ondemand_video_black_24dp,0,0,0
        );

        String tag_groups = course.getTag();
        List<String> tag_list = Arrays.asList(tag_groups.split(","));
        courseViewHolder.tag.setTags(tag_list);
        courseViewHolder.total_video.setText(course.getTotal_video()+" " + mContext.getResources().getString(R.string.videos));
        Glide.with(mContext)
                .load(course.getCover_url())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(courseViewHolder.cover);

        courseViewHolder.title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                detailCourse(mContext, course.getId_course());
            }
        });
        courseViewHolder.cover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                detailCourse(mContext, course.getId_course());
            }
        });
    }

    private void detailCourse(Context mContext, String id_course) {
        Intent intent = new Intent(mContext, CourseDetail.class);
        intent.putExtra("id_course", id_course);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(intent);
    }

    @Override
    public int getItemCount() {
        return courseList.size();
    }


}
