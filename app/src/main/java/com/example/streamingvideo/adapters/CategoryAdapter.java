package com.example.streamingvideo.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.streamingvideo.R;
import com.example.streamingvideo.event.EventGetCourseByCategory;
import com.example.streamingvideo.fragments.ExploreFragment;
import com.example.streamingvideo.models.Category;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder> {

    private List<Category> categoryList;
    private Context mContext;


    public CategoryAdapter(List<Category> categoryList, Context mContext) {
        this.categoryList = categoryList;
        this.mContext = mContext;
    }

    public class CategoryViewHolder extends RecyclerView.ViewHolder{
        TextView name;
        CardView card_category;
        public CategoryViewHolder(View itemView) {
            super(itemView);
            name                = itemView.findViewById(R.id.name);
            card_category       = itemView.findViewById(R.id.card_category);
        }
    }

    @Override
    public CategoryAdapter.CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_card_categories, parent, false);

        return new CategoryViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder categoryViewHolder, int i) {
        final Category category = categoryList.get(i);
        categoryViewHolder.name.setText(category.getName());
        categoryViewHolder.card_category.setCardBackgroundColor(Color.parseColor(category.getColor()));
        categoryViewHolder.card_category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new EventGetCourseByCategory(category.getId_category()));
            }
        });
    }

//    private void detailCourse(Context mContext, int id_course) {
//        Intent intent = new Intent(mContext, CourseDetail.class);
//        intent.putExtra("id_course", id_course);
//        mContext.startActivity(intent);
//
//    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }


}
