package com.example.streamingvideo.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.geolo.library.taggroup.GeoloTagGroup;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.streamingvideo.R;
import com.example.streamingvideo.detailactivity.CourseDetail;
import com.example.streamingvideo.models.Course;
import com.example.streamingvideo.models.ListLibraries;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class ListLibrariesAdapter extends RecyclerView.Adapter<ListLibrariesAdapter.ListLibrariesViewHolder> {

    private List<ListLibraries> listLibrariesList;
    private Context mContext;


    public ListLibrariesAdapter(List<ListLibraries> listLibrariesList, Context mContext) {
        this.listLibrariesList = listLibrariesList;
        this.mContext = mContext;
    }

    public class ListLibrariesViewHolder extends RecyclerView.ViewHolder {
        TextView name, implement;
        CardView card_view;
        public ListLibrariesViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            implement = itemView.findViewById(R.id.implement);
            card_view = itemView.findViewById(R.id.card_view);
        }
    }

    @Override
    public ListLibrariesAdapter.ListLibrariesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_card_library_apps, parent, false);

        return new ListLibrariesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ListLibrariesViewHolder listLibrariesViewHolder, int i) {
        final ListLibraries listLibraries = listLibrariesList.get(i);
        listLibrariesViewHolder.name.setText(listLibraries.getName());
        listLibrariesViewHolder.implement.setText(listLibraries.getImplement());
        listLibrariesViewHolder.card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openLinkToBrowser(mContext, listLibraries.getLink());
            }
        });
    }

    private void openLinkToBrowser(Context mContext, String link) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
        browserIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(browserIntent);
    }

    @Override
    public int getItemCount() {
        return listLibrariesList.size();
    }


}
