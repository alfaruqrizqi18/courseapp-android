package com.example.streamingvideo.adapters;

import android.content.Context;
import android.content.Intent;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.streamingvideo.R;
import com.example.streamingvideo.detailactivity.VideoDetail;
import com.example.streamingvideo.models.Video;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class VideoCourseAdapter extends RecyclerView.Adapter<VideoCourseAdapter.VideoCourseViewHolder> {

    private List<Video> videoList;
    private Context mContext;
//    MediaMetadataRetriever retriever = new MediaMetadataRetriever();
//    long timeInMillisec;
//    String time;


    public VideoCourseAdapter(List<Video> videoList, Context mContext) {
        this.videoList = videoList;
        this.mContext = mContext;
    }

    public class VideoCourseViewHolder extends RecyclerView.ViewHolder{
        TextView title, views, video_duration;
        CardView card_view;
        ImageView video_played;
        public VideoCourseViewHolder(View itemView) {
            super(itemView);
            views               = itemView.findViewById(R.id.views);
            title               = itemView.findViewById(R.id.title);
            card_view           = itemView.findViewById(R.id.card_view);
            video_played        = itemView.findViewById(R.id.video_played);
//            video_duration      = itemView.findViewById(R.id.video_duration);
        }
    }

    @Override
    public VideoCourseAdapter.VideoCourseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_card_video_course, parent, false);
        return new VideoCourseViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull VideoCourseViewHolder videoCourseViewHolder, int i) {
        final Video video = videoList.get(i);
        videoCourseViewHolder.title.setText((i+1) + ". " + video.getTitle());
        videoCourseViewHolder.views.setText(String.valueOf(video.getViews()) +" " + mContext.getResources().getString(R.string.views));
        if (video.isIs_played() == true) {
            videoCourseViewHolder.video_played.setVisibility(View.VISIBLE);
        } else {
            videoCourseViewHolder.video_played.setVisibility(View.GONE);
        }
        videoCourseViewHolder.title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                detailVideo(mContext, video.getId_video(), video.getVideo_url());
            }
        });
        videoCourseViewHolder.card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                detailVideo(mContext, video.getId_video(), video.getVideo_url());
            }
        });
    }

    private void detailVideo(Context mContext, String id_video, String video_url) {
        Intent intent = new Intent(mContext, VideoDetail.class);
        intent.putExtra("id_video", id_video);
        intent.putExtra("video_url", video_url);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(intent);
    }

    @Override
    public int getItemCount() {
        return videoList.size();
    }


}
