package com.example.streamingvideo.authactivity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.streamingvideo.MainActivity;
import com.example.streamingvideo.R;
import com.example.streamingvideo.loading.Loading;
import com.example.streamingvideo.models.User;
import com.example.streamingvideo.multilanguage_helper.Language;
import com.example.streamingvideo.sharedpreference.SharefPreferenceManager;
import com.example.streamingvideo.url.Auth;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import life.sabujak.roundedbutton.RoundedButton;

public class LoginActivity extends AppCompatActivity {
    TextView registrasi;
    EditText username_or_email, password;
    RoundedButton login_button;
    final Loading loading = new Loading();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        AndroidNetworking.initialize(getApplicationContext());
        username_or_email = findViewById(R.id.username_or_email);
        password = findViewById(R.id.password);
        login_button = findViewById(R.id.login_button);
        registrasi = findViewById(R.id.registrasi);

        File f = new File(
                "/data/data/"+getPackageName()+"/shared_prefs/"+SharefPreferenceManager.SHARED_PREF_NAME+".xml");
        if (!f.exists()) {
            new Language().setLanguangeRecreate(this, this.getResources().getString(R.string.default_lang));
        } else {
            String lang = SharefPreferenceManager.getInstance(LoginActivity.this).showSession().get("LANGUAGE");
            new Language().setLanguangeRecreate(LoginActivity.this, lang);
        }

        registrasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                startActivity(new Intent(getApplicationContext(), RegisterActivity.class));
            }
        });
        login_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (username_or_email.getText().toString().isEmpty() || password.getText().toString().isEmpty()) {
                    loading.alertDialogWithPositiveButton(LoginActivity.this,
                            LoginActivity.this.getResources().getString(R.string.warning),
                            LoginActivity.this.getResources().getString(R.string.credential_cant_be_empty));
                } else {
                    login(username_or_email.getText().toString(), password.getText().toString());
                }
            }
        });
    }

    public void login(String username_or_email, String password){
        loading.spinnerLoading(this,
                this.getResources().getString(R.string.validating),
                this.getResources().getString(R.string.please_wait));
        AndroidNetworking.post(Auth.login)
                .addBodyParameter("username_or_email", username_or_email)
                .addBodyParameter("password", password)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String message = response.getString("message");
                            if (message.equals("all_request_correct")) {

                                JSONObject data = response.getJSONObject("data");
                                User user = new User (
                                        data.getString("id_user"),
                                        data.getString("first_name"),
                                        data.getString("last_name"),
                                        data.getString("email"),
                                        data.getString("username"),
                                        data.getString("language"),
                                        data.getString("created_at")
                                );
                                SharefPreferenceManager.getInstance(getApplicationContext()).onLogin(user);
                                finish();
                                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                                loading.dismissLoading();

                            } else if (message.equals("username_or_password_not_correct")) {
                                loading.dismissLoading();
                                loading.alertDialogWithPositiveButton(LoginActivity.this,
                                        LoginActivity.this.getResources().getString(R.string.warning),
                                        LoginActivity.this.getResources().getString(R.string.username_or_password_incorrect));

                            } else if (message.equals("email_or_password_not_correct")){
                                loading.dismissLoading();
                                loading.alertDialogWithPositiveButton(LoginActivity.this,
                                        LoginActivity.this.getResources().getString(R.string.warning),
                                        LoginActivity.this.getResources().getString(R.string.email_or_password_incorrect));

                            } else {
                                loading.dismissLoading();
                                loading.alertDialogWithPositiveButton(LoginActivity.this,
                                        LoginActivity.this.getResources().getString(R.string.warning),
                                        LoginActivity.this.getResources().getString(R.string.creadential_no_matches));

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });
    }

}
