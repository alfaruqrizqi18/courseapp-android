package com.example.streamingvideo.authactivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.streamingvideo.MainActivity;
import com.example.streamingvideo.R;
import com.example.streamingvideo.loading.Loading;
import com.example.streamingvideo.models.User;
import com.example.streamingvideo.multilanguage_helper.Language;
import com.example.streamingvideo.sharedpreference.SharefPreferenceManager;
import com.example.streamingvideo.url.Auth;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import life.sabujak.roundedbutton.RoundedButton;

public class RegisterActivity extends AppCompatActivity {
    TextView login;
    EditText first_name, last_name, username, email, password;
    RoundedButton register_button;
    final Loading loading = new Loading();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        File f = new File(
                "/data/data/"+getPackageName()+"/shared_prefs/"+SharefPreferenceManager.SHARED_PREF_NAME.toString()+".xml");
        if (!f.exists()) {
            new Language().setLanguangeRecreate(this, this.getResources().getString(R.string.default_lang));
        } else {
            String lang = SharefPreferenceManager.getInstance(this).showSession().get("LANGUAGE");
            new Language().setLanguangeRecreate(this, lang);
        }

        AndroidNetworking.initialize(getApplicationContext());
        login = findViewById(R.id.login);
        first_name = findViewById(R.id.first_name);
        last_name = findViewById(R.id.last_name);
        username = findViewById(R.id.username);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        register_button = findViewById(R.id.register_button);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            }
        });

        register_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
                if (first_name.getText().toString().isEmpty()
                        || last_name.getText().toString().isEmpty()
                        || email.getText().toString().isEmpty()
                        || username.getText().toString().isEmpty()
                        || password.getText().toString().isEmpty()) {
                    loading.alertDialogWithPositiveButton(RegisterActivity.this,
                            RegisterActivity.this.getResources().getString(R.string.warning),
                            RegisterActivity.this.getResources().getString(R.string.please_fill_all_register_form));
                } else if (!email.getText().toString().matches(emailPattern)) {
                    loading.alertDialogWithPositiveButton(RegisterActivity.this,
                            RegisterActivity.this.getResources().getString(R.string.warning),
                            RegisterActivity.this.getResources().getString(R.string.use_valid_email));
                } else {
                    register();
                }

            }
        });
    }


    private void register() {
        loading.spinnerLoading(this,
                this.getResources().getString(R.string.registering),
                this.getResources().getString(R.string.please_wait));
        AndroidNetworking.post(Auth.register)
                .addBodyParameter("first_name", first_name.getText().toString().trim())
                .addBodyParameter("last_name", last_name.getText().toString().trim())
                .addBodyParameter("username", username.getText().toString().trim())
                .addBodyParameter("email", email.getText().toString().trim())
                .addBodyParameter("password", password.getText().toString().trim())
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String message = response.getString("message");
                            if (message.equals("all_request_correct")) {

                                JSONObject data = response.getJSONObject("data");
                                User user = new User(
                                        data.getString("id_user"),
                                        data.getString("first_name"),
                                        data.getString("last_name"),
                                        data.getString("email"),
                                        data.getString("username"),
                                        data.getString("language"),
                                        data.getString("created_at")
                                );
                                SharefPreferenceManager.getInstance(getApplicationContext()).onLogin(user);

                                loading.dismissLoading();
                                final AlertDialog.Builder alertDialogBuilder;
                                alertDialogBuilder = new AlertDialog.Builder(RegisterActivity.this);
                                alertDialogBuilder.setTitle(RegisterActivity.this.getResources().getString(R.string.congratulations));
                                alertDialogBuilder
                                        .setMessage(RegisterActivity.this.getResources().getString(R.string.register_success))
                                        .setCancelable(false)
                                        .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog,int id) {
                                                finish();
                                                startActivity(new Intent(RegisterActivity.this, MainActivity.class));
                                            }
                                        });
                                alertDialogBuilder.show();

                            } else if (message.equals("email_duplicate")) {
                                loading.dismissLoading();
                                loading.alertDialogWithPositiveButton(RegisterActivity.this,
                                        RegisterActivity.this.getResources().getString(R.string.warning),
                                        RegisterActivity.this.getResources().getString(R.string.email_duplicate));

                            } else if (message.equals("username_duplicate")) {
                                loading.dismissLoading();
                                loading.alertDialogWithPositiveButton(RegisterActivity.this,
                                        RegisterActivity.this.getResources().getString(R.string.warning),
                                        RegisterActivity.this.getResources().getString(R.string.username_duplicate));

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });
    }
}
