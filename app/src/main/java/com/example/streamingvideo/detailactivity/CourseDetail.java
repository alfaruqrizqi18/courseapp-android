package com.example.streamingvideo.detailactivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Handler;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.streamingvideo.R;
import com.example.streamingvideo.customviewpager.CustomViewPager;
import com.example.streamingvideo.fragments.detailcourse.AboutCourse;
import com.example.streamingvideo.fragments.detailcourse.VideoCourse;
import com.example.streamingvideo.loading.Loading;
import com.example.streamingvideo.models.Course;
import com.example.streamingvideo.multilanguage_helper.Language;
import com.example.streamingvideo.sharedpreference.SharefPreferenceManager;
import com.example.streamingvideo.url.All;
import com.example.streamingvideo.url.BaseUrl;
import com.like.LikeButton;
import com.like.OnLikeListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import life.sabujak.roundedbutton.RoundedButton;

public class CourseDetail extends AppCompatActivity {
    public boolean is_enroll, is_favorite, is_finish;
    final Loading loading = new Loading();
    String id_user = SharefPreferenceManager.getInstance(this).showSession().get("ID_USER");
    String id_course;
    int temp_category_color;
    public String temp_title, temp_category_name, temp_cover, summary, start_at, end_at, estimated_time_to_learn, tag, created_at;
    ImageView cover;
    TextView title, category_name;
    CardView card_category;
    TabLayout tabLayout;
    CustomViewPager viewPager;
    Toolbar toolbar;
    CollapsingToolbarLayout collapsingToolbar;
    LikeButton favorite_button;
    RoundedButton enroll_button, finish_button;
    CoordinatorLayout container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_detail);
        String lang = SharefPreferenceManager.getInstance(this).showSession().get("LANGUAGE");
        new Language().setLanguangeRecreate(this, lang);

        id_course = getIntent().getStringExtra("id_course");
        toolbar = findViewById(R.id.toolbar);
        enroll_button = findViewById(R.id.enroll_button);
        finish_button = findViewById(R.id.finish_button);
        collapsingToolbar = findViewById(R.id.collapsingToolbar);
        viewPager = findViewById(R.id.viewPager);
        tabLayout = findViewById(R.id.tabs);
        title = findViewById(R.id.title);
        cover = findViewById(R.id.cover);
        category_name = findViewById(R.id.category_name);
        card_category = findViewById(R.id.card_category);
        favorite_button = findViewById(R.id.favorite_button);
        container = findViewById(R.id.container);

        starterPack();

    }

    private void starterPack() {
        setupToolbar();
        setupFullscreen();
        getDetailCourse();
        checkIsFavorite();
        setupFavoriteButton();
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        collapsingToolbar.setTitleEnabled(false);
        toolbar.setTitle(null);
        toolbar.setNavigationIcon(getApplication().getResources().getDrawable(R.drawable.ic_chevron_left_white_24dp));
    }

    private void setIsFinish(boolean forIsFinish) {
        is_finish = forIsFinish;
        if (is_enroll == false && is_finish == false) {
            finish_button.setVisibility(View.GONE);
        } else if (is_enroll == true && is_finish == false) {
            finish_button.setVisibility(View.VISIBLE);
            setupFinishButton();
        } else if (is_enroll == true && is_finish == true) {
            finish_button.setVisibility(View.GONE);
        }
    }

    private void setupFinishButton() {
        finish_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder alertDialogBuilder;
                alertDialogBuilder = new AlertDialog.Builder(CourseDetail.this);
                alertDialogBuilder.setTitle(CourseDetail.this.getResources().getString(R.string.confirmation));
                alertDialogBuilder
                        .setMessage(CourseDetail.this.getResources().getString(R.string.do_you_want_finish))
                        .setCancelable(false)
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        })
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                finishButton();
                            }
                        });
                alertDialogBuilder.show();
            }
        });
    }

    private void setIsFavorite(boolean forIsfavorite) {
        is_favorite = forIsfavorite;
        if (is_favorite == true) {
            favorite_button.setLiked(true);
        } else {
            favorite_button.setLiked(false);
        }
    }


    private void setIsEnroll(boolean forIsEnroll) {
        is_enroll = forIsEnroll;
        setupHideShowEnrollButton();
        setupViewPagerPaging();
        enableDisableTabLayout();
    }

    private void setupFullscreen() {
        Window w = getWindow(); // in Activity's onCreate() for instance
        w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        w.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }


    private void setupEnrollButton() {
        enroll_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder alertDialogBuilder;
                alertDialogBuilder = new AlertDialog.Builder(CourseDetail.this);
                alertDialogBuilder.setTitle(CourseDetail.this.getResources().getString(R.string.confirmation));
                alertDialogBuilder
                        .setMessage(CourseDetail.this.getResources().getString(R.string.do_you_want_enroll))
                        .setCancelable(false)
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        })
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                enrollCourse();
                            }
                        });
                alertDialogBuilder.show();
            }
        });
    }

    private void setupFavoriteButton() {
        favorite_button.setOnLikeListener(new OnLikeListener() {
            @Override
            public void liked(LikeButton likeButton) {
//                Toast.makeText(CourseDetail.this, "Liked", Toast.LENGTH_SHORT).show();
                favoriteCourse();
            }

            @Override
            public void unLiked(LikeButton likeButton) {
//                Toast.makeText(CourseDetail.this, "Unliked", Toast.LENGTH_SHORT).show();
                unfavoriteCourse();
            }
        });
    }

    private void setupViewPagerPaging() {
        if (is_enroll == true) {
            viewPager.setPagingEnabled(true);
            enableDisableTabLayout();
        } else {
            viewPager.setPagingEnabled(false);
            enableDisableTabLayout();
        }
    }

    private void setupHideShowEnrollButton() {
        if (is_enroll == true) {
            enroll_button.setVisibility(View.GONE);
        } else {
            enroll_button.setVisibility(View.VISIBLE);
            setupEnrollButton();
        }
    }

    private void finishButton() {
        loading.spinnerLoading(this,
                CourseDetail.this.getResources().getString(R.string.finishing),
                this.getResources().getString(R.string.please_wait));
        AndroidNetworking.post(All.finish_course)
                .addBodyParameter("id_user", id_user)
                .addBodyParameter("id_course", String.valueOf(id_course))
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String message = response.getString("message");
                            loading.dismissLoading();
                            if (message.equals("success_to_finish")) {
                                loading.alertDialogWithPositiveButton(CourseDetail.this,
                                        CourseDetail.this.getResources().getString(R.string.congratulations),
                                        CourseDetail.this.getResources().getString(R.string.you_have_completed) +
                                                " \"" + temp_title + "\"");
                                setIsFinish(true);
                            } else {
                                loading.alertDialogWithPositiveButton(CourseDetail.this,
                                        CourseDetail.this.getResources().getString(R.string.oops),
                                        CourseDetail.this.getResources().getString(R.string.sorry_you_cant_finish_the_course) +
                                                " \"" + temp_title + "\"");
                                setIsFinish(false);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });
    }

    private void enrollCourse() {
        loading.spinnerLoading(this,
                CourseDetail.this.getResources().getString(R.string.enrolling),
                this.getResources().getString(R.string.please_wait));
        AndroidNetworking.post(All.enroll_course)
                .addBodyParameter("id_user", id_user)
                .addBodyParameter("id_course", String.valueOf(id_course))
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String message = response.getString("message");
                            if (message.equals("success")) {
                                loading.dismissLoading();
                                loading.alertDialogWithPositiveButton(CourseDetail.this,
                                        CourseDetail.this.getResources().getString(R.string.congratulations),
                                        CourseDetail.this.getResources().getString(R.string.enrolls) + " \""
                                                + temp_title +
                                                "\" " + CourseDetail.this.getResources().getString(R.string.success)+". "
                                                + CourseDetail.this.getResources().getString(R.string.now_you_can_learn));
                                setIsEnroll(true);
                                setIsFinish(false);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });
    }

    private void favoriteCourse() {
        AndroidNetworking.initialize(getApplicationContext());
        AndroidNetworking.post(All.favorite_course)
                .addBodyParameter("id_user", id_user)
                .addBodyParameter("id_course", id_course)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
//                        Toast.makeText(getApplicationContext(), "Id user : "+id_user+" - Id course : "+id_course, Toast.LENGTH_SHORT).show();
                        setIsFavorite(true);
                        Toast.makeText(getApplicationContext(),
                                "\"" + temp_title + "\" " + CourseDetail.this.getResources().getString(R.string.has_been_added_to_fav_course),
                                Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });
    }

    private void unfavoriteCourse() {
        AndroidNetworking.initialize(getApplicationContext());
        AndroidNetworking.post(All.delete_favorite_course)
                .addBodyParameter("id_user", id_user)
                .addBodyParameter("id_course", id_course)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
//                        Toast.makeText(getApplicationContext(), "Id user : "+id_user+" - Id course : "+id_course, Toast.LENGTH_SHORT).show();
                        setIsFavorite(false);
                        Toast.makeText(getApplicationContext(),
                                "\"" + temp_title + "\" " + CourseDetail.this.getResources().getString(R.string.has_been_removed_to_fav_course),
                                Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });
    }

    private void enableDisableTabLayout() {
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                final int index = tabLayout.getSelectedTabPosition();
                if (index == 1 && is_enroll == false || index == 2 && is_enroll == false) {
                    new Handler().postDelayed(
                            new Runnable() {
                                @Override
                                public void run() {
                                    loading.alertDialogWithPositiveButton(CourseDetail.this,
                                            CourseDetail.this.getResources().getString(R.string.warning),
                                            CourseDetail.this.getResources().getString(R.string.please_enroll));
                                    tabLayout.getTabAt(0).select();
                                }
                            }, 100);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void setupTabLayout(int color) {
        tabLayout.setSelectedTabIndicatorColor(color);
        tabLayout.setTabTextColors(getApplication().getResources().getColor(R.color.grey_active), color);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager, String title, String summary, String start_at, String end_at, String estimated_time_to_learn,
                                String tag, String created_at) {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

        Bundle bundleAboutCourse = new Bundle();
        bundleAboutCourse.putString("title", title);
        bundleAboutCourse.putString("summary", summary);
        bundleAboutCourse.putString("start_at", start_at);
        bundleAboutCourse.putString("end_at", end_at);
        bundleAboutCourse.putString("estimated_time_to_learn", estimated_time_to_learn);
        bundleAboutCourse.putString("tag", tag);
        bundleAboutCourse.putString("created_at", created_at);
        AboutCourse aboutCourse = new AboutCourse();
        aboutCourse.setArguments(bundleAboutCourse);
        viewPagerAdapter.addFragment(aboutCourse, this.getResources().getString(R.string.about_course));

        Bundle bundleVideoCourse = new Bundle();
        bundleVideoCourse.putString("id_course", String.valueOf(id_course));
        VideoCourse videoCourse = new VideoCourse();
        videoCourse.setArguments(bundleVideoCourse);
        viewPagerAdapter.addFragment(videoCourse, this.getResources().getString(R.string.Videos));

//        viewPagerAdapter.addFragment(new NotificationFragment(), "Misc");
        viewPager.setAdapter(viewPagerAdapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(android.support.v4.app.Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }


        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    private void getDetailCourse() {
        AndroidNetworking.initialize(getApplicationContext());
        onLoadDetailCourse();
        AndroidNetworking.get(All.view_course + id_course)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray data = response.getJSONArray("data");
                            JSONObject info = data.getJSONObject(0);

                            temp_title = info.getString("title");
                            temp_cover = BaseUrl.base_url_web + info.getString("cover_url");
                            temp_category_name = info.getJSONObject("category").getString("name");
                            temp_category_color = Color.parseColor(info.getJSONObject("category").getString("color"));

                            summary = info.getString("summary");
                            estimated_time_to_learn = info.getString("estimated_time_to_learn");
                            start_at = info.getString("start_at");
                            end_at = info.getString("end_at");
                            created_at = info.getString("created_at");
                            tag = info.getString("tag");




                            /*
                                setupTabLayout berfungsi untuk mengatur warna tabIndicator dan warna tabText
                            */
                            setupTabLayout(temp_category_color);

                            /*
                                setupViewPager berfungsi untuk setting view pager dengan fragment
                                untuk parameter summary, start_at, end_at, estimated_time_to_learn, tag, created_at
                                digunakan untuk mengirim bundle data ke fragment about
                            */
                            setupViewPager(viewPager, temp_title, summary, start_at, end_at,
                                    estimated_time_to_learn, tag, created_at);


                            /*
                                settingCourseTitleImageCoverCategory berfungsi untuk menampilkan title, cover, kategori
                                dan warna kategori ke layout xml
                            */
                            settingCourseTitleImageCoverCategory(temp_title, temp_cover, temp_category_name, temp_category_color);

                            /*
                                checkingIsEnroll berfungsi sebagai pengecek apakah course sudah di enroll oleh pengguna
                            */
                            checkIsEnroll();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });
    }

    private void settingCourseTitleImageCoverCategory(String temp_title, String temp_cover, String temp_category_name, int temp_category_color) {
        Glide.with(getApplicationContext())
                .load(temp_cover)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(cover);

        title.setText(temp_title);
        category_name.setText(temp_category_name);
        card_category.setCardBackgroundColor(temp_category_color);
    }

    private void checkIsEnroll() {
        String param = id_user + "/" + id_course;
        AndroidNetworking.get(All.check_enroll_course + param)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String message = response.getString("message");
                            JSONArray data = response.getJSONArray("data");
                            if (message.equals("data_found")) {
                                String course_status = data.getJSONObject(0).getString("course_status");
                                if (course_status.equals("on_going")) {
                                    setIsEnroll(true);
                                    setIsFinish(false);
                                } else {
                                    setIsEnroll(true);
                                    setIsFinish(true);
                                }
                            } else {
                                setIsFinish(false);
                                setIsEnroll(false);
                            }

                            onLoadDetailCourseFinish();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });
    }

    private void checkIsFavorite() {
        String param = id_user + "/" + id_course;
        AndroidNetworking.get(All.check_favorite_course + param)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String message = response.getString("message");
                            if (message.equals("data_found")) {
                                setIsFavorite(true);
                            } else if (message.equals("data_not_found")) {
                                setIsFavorite(false);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });
    }

    private void onLoadDetailCourse() {
        loading.spinnerLoading(this,
                this.getResources().getString(R.string.find_your_course),
                this.getResources().getString(R.string.please_wait));
        container.setVisibility(View.INVISIBLE);
    }

    private void onLoadDetailCourseFinish() {
        loading.dismissLoading();
        container.setVisibility(View.VISIBLE);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
