package com.example.streamingvideo.event;

public class EventGetCourseByCategory {

    public String id_category;

    public EventGetCourseByCategory(String id_category) {
        this.id_category = id_category;
    }

    public String getId_category() {
        return id_category;
    }
}
