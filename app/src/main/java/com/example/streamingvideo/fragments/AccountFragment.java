package com.example.streamingvideo.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.streamingvideo.R;
import com.example.streamingvideo.activity.AboutApps;
import com.example.streamingvideo.activity.AccountForm;
import com.example.streamingvideo.multilanguage_helper.Language;
import com.example.streamingvideo.sharedpreference.SharefPreferenceManager;
import com.example.streamingvideo.url.All;

import org.json.JSONException;
import org.json.JSONObject;

public class AccountFragment extends Fragment {
    CardView card_account_form, card_logout, card_about;
    TextView profile_name, ongoing_course, favorite_course, completed_course;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_account, container, false);

        card_account_form = view.findViewById(R.id.card_account_form);
        card_logout = view.findViewById(R.id.card_logout);
        card_about = view.findViewById(R.id.card_about);

        profile_name = view.findViewById(R.id.profile_name);
        ongoing_course = view.findViewById(R.id.ongoing_course);
        favorite_course = view.findViewById(R.id.favorite_course);
        completed_course = view.findViewById(R.id.completed_course);
        setClick();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreviewUser();
        String lang = SharefPreferenceManager.getInstance(getContext()).showSession().get("LANGUAGE");
        new Language().setLanguangeRecreate(getContext(), lang);
        profile_name.setText(
                SharefPreferenceManager.getInstance(getContext()).showSession().get("FIRST_NAME") +" "+
                        SharefPreferenceManager.getInstance(getContext()).showSession().get("LAST_NAME"));
    }

    private void setClick() {
        card_account_form.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), AccountForm.class));
            }
        });
        card_about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), AboutApps.class));
            }
        });
        card_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder alertDialogBuilder;
                alertDialogBuilder = new AlertDialog.Builder(getContext());
                alertDialogBuilder.setTitle(getContext().getResources().getString(R.string.confirmation));
                alertDialogBuilder
                        .setMessage(getContext().getResources().getString(R.string.do_you_want_logout))
                        .setCancelable(false)
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        })
                        .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                SharefPreferenceManager.getInstance(getContext()).logout();
                            }
                        });
                alertDialogBuilder.show();
            }
        });
    }

    public void getPreviewUser() {
        String id_user = SharefPreferenceManager.getInstance(getContext()).showSession().get("ID_USER");
        AndroidNetworking.get(All.index_user_preview + id_user)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String status = response.getString("status");
                            completed_course.setText(response.getString("completed_course"));
                            ongoing_course.setText(response.getString("ongoing_course"));
                            favorite_course.setText(response.getString("favorite_course"));
                            JSONObject data = response.getJSONObject("data");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });
    }
}
