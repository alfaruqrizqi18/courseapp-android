package com.example.streamingvideo.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.streamingvideo.R;
import com.example.streamingvideo.adapters.AllCourseFromExploreAdapter;
import com.example.streamingvideo.adapters.CategoryAdapter;
import com.example.streamingvideo.event.EventGetCourseByCategory;
import com.example.streamingvideo.models.Category;
import com.example.streamingvideo.models.Course;
import com.example.streamingvideo.multilanguage_helper.Language;
import com.example.streamingvideo.sharedpreference.SharefPreferenceManager;
import com.example.streamingvideo.url.All;
import com.example.streamingvideo.url.BaseUrl;
import com.facebook.shimmer.ShimmerFrameLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ExploreFragment extends Fragment {

    RecyclerView rv_categories, rv_course;
    EditText search_course;
    ShimmerFrameLayout mShimmerViewContainer, mShimmerViewContainerCourse;
    View search_not_found_layout;
    TextView search_not_found_text;
    List<Course> courseList = new ArrayList<>();
    CardView reset_button;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_explore, container, false);
        String lang = SharefPreferenceManager.getInstance(getContext()).showSession().get("LANGUAGE");
        new Language().setLanguangeRecreate(getContext(), lang);

        rv_categories           = view.findViewById(R.id.rv_categories);
        rv_course               = view.findViewById(R.id.rv_course);
        mShimmerViewContainer   = view.findViewById(R.id.shimmer_view_container);
        mShimmerViewContainerCourse   = view.findViewById(R.id.shimmer_view_container_course);
        reset_button            = view.findViewById(R.id.reset_button);
        search_course           = view.findViewById(R.id.search_course);
        search_not_found_layout = view.findViewById(R.id.search_not_found_layout);
        search_not_found_text   = search_not_found_layout.findViewById(R.id.search_not_found_text);

        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        LinearLayoutManager layoutManagerCourse
                = new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        rv_categories.setLayoutManager(layoutManager);
        rv_categories.setNestedScrollingEnabled(false);
        rv_course.setLayoutManager(layoutManagerCourse);
        rv_course.setNestedScrollingEnabled(false);

        setupSnapHelper();
        hideSearchNotFound();
        queryOnSearch();
        setupResetButton();
        getCategories();
        return view;
    }



    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    private void setupResetButton(){
        reset_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeKeyboard();
                search_course.setText(null);
                search_course.clearFocus();
                getCourse();
            }
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(EventGetCourseByCategory eventGetCourseByCategory) {
        getCourseByIdCategory(eventGetCourseByCategory.getId_category());
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
        getCourse();
    }



    private void hideSearchNotFound() {
        search_not_found_layout.setVisibility(View.GONE);
    }

    private void showSearchNotFound(String query) {
        search_not_found_layout.setVisibility(View.VISIBLE);
        search_not_found_text.setText(getString(R.string.search_not_found) + " " + "\""+ query+ "\".");
    }

    private void closeKeyboard() {
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void setupSnapHelper(){
        SnapHelper snapHelper = new LinearSnapHelper();
        snapHelper.attachToRecyclerView(rv_categories);
    }

    private void queryOnSearch(){
        search_course.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    searchCourseByQuery();
                    closeKeyboard();
                    return true;
                }
                return false;
            }
        });
    }

    private void stopShimmerCourse(){
        mShimmerViewContainerCourse.stopShimmer();
        mShimmerViewContainerCourse.setVisibility(View.GONE);
    }

    private void startShimmerCourse(){
        mShimmerViewContainerCourse.startShimmer();
        mShimmerViewContainerCourse.setVisibility(View.VISIBLE);
    }


    private void stopShimmerCategories(){
        mShimmerViewContainer.stopShimmer();
        mShimmerViewContainer.setVisibility(View.GONE);
    }

    private void startShimmerCategories(){
        mShimmerViewContainer.startShimmer();
        mShimmerViewContainer.setVisibility(View.VISIBLE);
    }

    public void getCategories(){
        startShimmerCategories();
        AndroidNetworking.get(All.index_category)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            List<Category> categoryList = new ArrayList<>();
                            String status = response.getString("status");
                            JSONArray data = response.getJSONArray("data");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject info = data.getJSONObject(i);
                                Category category = new Category();
                                category.setId_category(info.getString("id_category"));
                                category.setName(info.getString("name"));
                                category.setColor(info.getString("color"));
                                category.setCourse_length(info.getJSONArray("course").length());
                                categoryList.add(category);
                            }
                            CategoryAdapter categoryAdapter = new CategoryAdapter(categoryList, getContext());
                            rv_categories.setAdapter(categoryAdapter);
                            stopShimmerCategories();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });
    }

    public void getCourse(){
        courseList.clear();
        startShimmerCourse();
        hideSearchNotFound();
        AndroidNetworking.get(All.index_course)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String status = response.getString("status");
                            JSONArray data = response.getJSONArray("data");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject info = data.getJSONObject(i);
                                Course course = new Course();
                                course.setId_course(info.getString("id_course"));
                                course.setTitle(info.getString("title"));
                                course.setStart_at(info.getString("start_at"));
                                course.setEnd_at(info.getString("end_at"));
                                course.setTag(info.getString("tag"));
                                course.setCategory_name(info.getJSONObject("category").getString("name"));
                                course.setColor(info.getJSONObject("category").getString("color"));
                                course.setCover_url(BaseUrl.base_url_web + info.getString("cover_url"));
                                course.setTotal_video(info.getJSONArray("video").length());
                                courseList.add(course);
                            }
                            AllCourseFromExploreAdapter allCourseFromExploreAdapter =
                                    new AllCourseFromExploreAdapter(courseList, getContext());

                            rv_course.setAdapter(allCourseFromExploreAdapter);
                            stopShimmerCourse();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });
    }

    public void getCourseByIdCategory(String id_category){
        courseList.clear();
        hideSearchNotFound();
        startShimmerCourse();
        String query = All.index_course + "?id_category="+id_category;
        AndroidNetworking.get(query)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String status = response.getString("status");
                            JSONArray data = response.getJSONArray("data");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject info = data.getJSONObject(i);
                                Course course = new Course();
                                course.setId_course(info.getString("id_course"));
                                course.setTitle(info.getString("title"));
                                course.setStart_at(info.getString("start_at"));
                                course.setEnd_at(info.getString("end_at"));
                                course.setTag(info.getString("tag"));
                                course.setCategory_name(info.getJSONObject("category").getString("name"));
                                course.setColor(info.getJSONObject("category").getString("color"));
                                course.setCover_url(BaseUrl.base_url_web + info.getString("cover_url"));
                                course.setTotal_video(info.getJSONArray("video").length());
                                courseList.add(course);
                            }
                            AllCourseFromExploreAdapter allCourseFromExploreAdapter =
                                    new AllCourseFromExploreAdapter(courseList, getContext());

                            rv_course.setAdapter(allCourseFromExploreAdapter);
                            stopShimmerCourse();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });
    }

    public void searchCourseByQuery(){
        startShimmerCourse();
        courseList.clear();
        final String query = search_course.getText().toString().trim();
        String url_with_query = All.index_course + "?query=" + query;
        AndroidNetworking.get(url_with_query)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray data = response.getJSONArray("data");
                            if (data.length() > 0 ) {
                                for (int i = 0; i < data.length(); i++) {
                                    JSONObject info = data.getJSONObject(i);
                                    Course course = new Course();
                                    course.setId_course(info.getString("id_course"));
                                    course.setTitle(info.getString("title"));
                                    course.setStart_at(info.getString("start_at"));
                                    course.setEnd_at(info.getString("end_at"));
                                    course.setTag(info.getString("tag"));
                                    course.setCategory_name(info.getJSONObject("category").getString("name"));
                                    course.setColor(info.getJSONObject("category").getString("color"));
                                    course.setCover_url(BaseUrl.base_url_web + info.getString("cover_url"));
                                    course.setTotal_video(info.getJSONArray("video").length());
                                    courseList.add(course);
                                }
                                AllCourseFromExploreAdapter allCourseFromExploreAdapter =
                                        new AllCourseFromExploreAdapter(courseList, getContext());

                                rv_course.setAdapter(allCourseFromExploreAdapter);
                                stopShimmerCourse();
                                hideSearchNotFound();
                            } else {
                                stopShimmerCourse();
                                showSearchNotFound(query);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });
    }
}
