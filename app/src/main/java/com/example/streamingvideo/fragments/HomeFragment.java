package com.example.streamingvideo.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.streamingvideo.R;
import com.example.streamingvideo.activity.AllCompletedCourses;
import com.example.streamingvideo.activity.AllFavoriteCourses;
import com.example.streamingvideo.activity.AllLatestCourses;
import com.example.streamingvideo.activity.AllOnGoingCourses;
import com.example.streamingvideo.activity.AllRecommendedCourses;
import com.example.streamingvideo.adapters.LatestCourseAdapter;
import com.example.streamingvideo.adapters.RecommendedCourseAdapter;
import com.example.streamingvideo.models.Course;
import com.example.streamingvideo.multilanguage_helper.Language;
import com.example.streamingvideo.sharedpreference.SharefPreferenceManager;
import com.example.streamingvideo.url.All;
import com.example.streamingvideo.url.BaseUrl;
import com.facebook.shimmer.ShimmerFrameLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends Fragment {
    RecyclerView rv_course, rv_recommended;
    TextView view_all_latest_course, view_all_recommended_course;
    Toolbar toolbar;
    ShimmerFrameLayout mShimmerViewContainer, mShimmerViewContainerRecommended;
    LinearLayout recommendation_not_found, new_comer_meesage, favorite_page_button, completed_page_button, on_going_page_button;
    List<Course> courseList = new ArrayList<>();
    List<Course> recommendedCourseList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        String lang = SharefPreferenceManager.getInstance(getContext()).showSession().get("LANGUAGE");
        new Language().setLanguangeRecreate(getContext(), lang);

        toolbar = view.findViewById(R.id.toolbar);
        on_going_page_button = view.findViewById(R.id.on_going_page_button);
        favorite_page_button = view.findViewById(R.id.favorite_page_button);
        completed_page_button = view.findViewById(R.id.completed_page_button);
        view_all_latest_course = view.findViewById(R.id.view_all_latest_course);
        view_all_recommended_course = view.findViewById(R.id.view_all_recommended_course);
        recommendation_not_found = view.findViewById(R.id.recommendation_not_found);
        new_comer_meesage = view.findViewById(R.id.new_comer_meesage);
        rv_course = view.findViewById(R.id.rv_course);
        rv_recommended = view.findViewById(R.id.rv_recommended);
        mShimmerViewContainer   = view.findViewById(R.id.shimmer_view_container);
        mShimmerViewContainerRecommended   = view.findViewById(R.id.shimmer_view_container_recommendation);
        LinearLayoutManager layoutManagerCourse
                = new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        rv_course.setLayoutManager(layoutManagerCourse);
        rv_course.setNestedScrollingEnabled(false);

        LinearLayoutManager layoutManagerRecommended
                = new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        rv_recommended.setLayoutManager(layoutManagerRecommended);
        rv_recommended.setNestedScrollingEnabled(false);

        starterPack();
        return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        AndroidNetworking.initialize(getActivity().getApplicationContext());
    }

    private void setupToolbar(){
        String first_name = SharefPreferenceManager.getInstance(getContext()).showSession().get("FIRST_NAME");
        toolbar.setTitle(getContext().getResources().getString(R.string.welcome)+" "+first_name);
    }

    @Override
    public void onResume() {
        super.onResume();
        getLatestCourse();
        getRecommendedCourse();
    }

    private void starterPack() {
        setupToolbar();
        recommendation_not_found.setVisibility(View.GONE);
        new_comer_meesage.setVisibility(View.GONE);
        SnapHelper snapHelperCourse = new LinearSnapHelper();
        SnapHelper snapHelperRecommendedCourse = new LinearSnapHelper();
        snapHelperCourse.attachToRecyclerView(rv_course);
        snapHelperRecommendedCourse
                .attachToRecyclerView(rv_recommended);
        viewAllOnClick();
        quickNavigation();
    }

    private void stopShimmer(){
        mShimmerViewContainer.stopShimmer();
        mShimmerViewContainer.setVisibility(View.GONE);
    }

    private void startShimmer(){
        mShimmerViewContainer.startShimmer();
        rv_course.setVisibility(View.GONE);
        mShimmerViewContainer.setVisibility(View.VISIBLE);
    }

    private void stopShimmerRecommended(){
        mShimmerViewContainerRecommended.stopShimmer();
        mShimmerViewContainerRecommended.setVisibility(View.GONE);
    }

    private void startShimmerRecommended(){
        mShimmerViewContainerRecommended.startShimmer();
        mShimmerViewContainerRecommended.setVisibility(View.VISIBLE);
    }

    private void viewAllOnClick() {
        view_all_latest_course.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), AllLatestCourses.class));
            }
        });
        view_all_recommended_course.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), AllRecommendedCourses.class));
            }
        });
    }

    private void quickNavigation(){
        favorite_page_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), AllFavoriteCourses.class));
            }
        });
        completed_page_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), AllCompletedCourses.class));
            }
        });
        on_going_page_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), AllOnGoingCourses.class));
            }
        });
    }

    public void getLatestCourse(){
        courseList.clear();
        startShimmer();
        AndroidNetworking.get(All.latest_course)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String status = response.getString("status");
                            JSONArray data = response.getJSONArray("data");
                            if (data.length() > 0) {
                                for (int i = 0; i < data.length(); i++) {
                                    JSONObject info = data.getJSONObject(i);
                                    Course course = new Course();
                                    course.setId_course(info.getString("id_course"));
                                    course.setTitle(info.getString("title"));
                                    course.setStart_at(info.getString("start_at"));
                                    course.setEnd_at(info.getString("end_at"));
                                    course.setTag(info.getString("tag"));
                                    course.setCategory_name(info.getJSONObject("category").getString("name"));
                                    course.setColor(info.getJSONObject("category").getString("color"));
                                    course.setCover_url(BaseUrl.base_url_web + info.getString("cover_url"));
                                    course.setTotal_video(info.getJSONArray("video").length());
                                    courseList.add(course);
                                }
                                rv_course.setVisibility(View.VISIBLE);
                                LatestCourseAdapter latestCourseAdapter = new LatestCourseAdapter(courseList, getContext());
                                rv_course.setAdapter(latestCourseAdapter);
                            } else {
                                rv_course.setVisibility(View.GONE);
                            }
                                stopShimmer();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });
    }

    public void getRecommendedCourse(){
        String id_user = SharefPreferenceManager.getInstance(getContext()).showSession().get("ID_USER");
        recommendedCourseList.clear();
        startShimmerRecommended();
        AndroidNetworking.get(All.recommended_course + id_user)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String message = response.getString("message");
                            if (message.equals("data_found")) {
                                JSONArray data = response.getJSONArray("recommended_course");
                                if (data.length() > 0) {
                                    for (int i = 0; i < data.length(); i++) {
                                        JSONObject info = data.getJSONObject(i);
                                        Course course = new Course();
                                        course.setId_course(info.getString("id_course"));
                                        course.setTitle(info.getString("title"));
                                        course.setStart_at(info.getString("start_at"));
                                        course.setEnd_at(info.getString("end_at"));
                                        course.setTag(info.getString("tag"));
                                        course.setCategory_name(info.getJSONObject("category").getString("name"));
                                        course.setColor(info.getJSONObject("category").getString("color"));
                                        course.setCover_url(BaseUrl.base_url_web + info.getString("cover_url"));
                                        course.setTotal_video(info.getJSONArray("video").length());
                                        recommendedCourseList.add(course);
                                    }
                                    rv_recommended.setVisibility(View.VISIBLE);
                                    RecommendedCourseAdapter recommendedCourseAdapter =
                                            new RecommendedCourseAdapter(recommendedCourseList, getContext());
                                    rv_recommended.setAdapter(recommendedCourseAdapter);
                                } else {
                                    new_comer_meesage.setVisibility(View.GONE);
                                    recommendation_not_found.setVisibility(View.VISIBLE);
                                    rv_recommended.setVisibility(View.GONE);
                                }
                            } else {
                                rv_recommended.setVisibility(View.GONE);
                                new_comer_meesage.setVisibility(View.VISIBLE);
                                recommendation_not_found.setVisibility(View.GONE);
                            }
                            stopShimmerRecommended();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });
    }
}
