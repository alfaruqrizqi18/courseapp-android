package com.example.streamingvideo.fragments.detailcourse;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.geolo.library.taggroup.GeoloTagGroup;
import com.example.streamingvideo.R;
import com.example.streamingvideo.multilanguage_helper.Language;
import com.example.streamingvideo.sharedpreference.SharefPreferenceManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class AboutCourse extends Fragment {
    TextView  summary, start_at, end_at, estimated_to_learn, created_at;
    GeoloTagGroup tag;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_about_course, container, false);
        String lang = SharefPreferenceManager.getInstance(getContext()).showSession().get("LANGUAGE");
        new Language().setLanguangeRecreate(getContext(), lang);

        String summary_val                  = getArguments().getString("summary");
        String start_at_val                 = getArguments().getString("start_at");
        String end_at_val                   = getArguments().getString("end_at");
        String estimated_time_to_learn_val  = getArguments().getString("estimated_time_to_learn");
        String tag_val                      = getArguments().getString("tag");
        String created_at_val               = getArguments().getString("created_at");

        summary                             = view.findViewById(R.id.summary);
        start_at                            = view.findViewById(R.id.start_at);
        end_at                              = view.findViewById(R.id.end_at);
        estimated_to_learn                  = view.findViewById(R.id.estimated_to_learn);
        created_at                          = view.findViewById(R.id.created_at);
        tag                                 = view.findViewById(R.id.tag);


        summary.setText(summary_val);
        estimated_to_learn.setText(estimated_time_to_learn_val);

        try {
            SimpleDateFormat sdfSource = new SimpleDateFormat("yyyy-mm-dd");
            SimpleDateFormat sdfDestination = new SimpleDateFormat("dd MMM yyyy");
            Date start_at_date = sdfSource.parse(start_at_val);
            Date end_at_date = sdfSource.parse(end_at_val);
            Date created_at_date = sdfSource.parse(created_at_val);
            start_at.setText(sdfDestination.format(start_at_date));
            end_at.setText(sdfDestination.format(end_at_date));
            created_at.setText(sdfDestination.format(created_at_date));

        } catch (ParseException e) {
            e.printStackTrace();
        }


        List<String> tag_list = Arrays.asList(tag_val.split(","));
        tag.setTags(tag_list);
        return view;
    }
}
