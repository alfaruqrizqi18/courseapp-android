package com.example.streamingvideo.fragments.detailcourse;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.streamingvideo.R;
import com.example.streamingvideo.adapters.VideoCourseAdapter;
import com.example.streamingvideo.models.Video;
import com.example.streamingvideo.multilanguage_helper.Language;
import com.example.streamingvideo.sharedpreference.SharefPreferenceManager;
import com.example.streamingvideo.url.All;
import com.example.streamingvideo.url.BaseUrl;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class VideoCourse extends Fragment {
    RecyclerView rv_video;
    String id_course;
    String id_user = SharefPreferenceManager.getInstance(getContext()).showSession().get("ID_USER");
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_video_course, container, false);
        String lang = SharefPreferenceManager.getInstance(getContext()).showSession().get("LANGUAGE");
        new Language().setLanguangeRecreate(getContext(), lang);

        rv_video = view.findViewById(R.id.rv_video);
        id_course = getArguments().getString("id_course");
//        starterPack();
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        getAllVideoByCourse();
    }

    private void setupRecyclerView() {
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rv_video.setLayoutManager(layoutManager);
        rv_video.setNestedScrollingEnabled(false);
    }

    private void getAllVideoByCourse(){
        AndroidNetworking.get(All.all_video_by_course + id_course)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        List<Video> videoList = new ArrayList<>();
                        JSONArray user_viewed;
                        try {
                            JSONArray data = response.getJSONArray("data");
                            for (int i = 0; i < data.length(); i++) {
                                Video videoCourse = new Video();
                                JSONObject info = data.getJSONObject(i);
                                videoCourse.setId_video(info.getString("id_video"));
                                videoCourse.setTitle(info.getString("title"));
                                videoCourse.setViews(info.getJSONArray("views").length());
                                user_viewed = info.getJSONArray("views");
                                videoCourse.setVideo_url(BaseUrl.base_url_web + info.getString("video_url"));
//                                Log.d("VIEWED", String.valueOf(user_viewed));
                                for (int j = 0; j < user_viewed.length(); j++) {
                                    JSONObject object_viewed = user_viewed.getJSONObject(j);
                                    String user_id_viewed = object_viewed.getString("id_user");
                                    String video_id_viewed = object_viewed.getString("id_video");

                                    if (user_id_viewed.equals(id_user) && video_id_viewed.equals(info.getString("id_video"))) {
                                        videoCourse.setIs_played(true);
                                    } else {
                                        videoCourse.setIs_played(false);
                                    }
                                }
                                videoList.add(videoCourse);
                                VideoCourseAdapter videoCourseAdapter = new VideoCourseAdapter(videoList, getContext());
                                rv_video.setAdapter(videoCourseAdapter);
                                setupRecyclerView();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });
    }

}
