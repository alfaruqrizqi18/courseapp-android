package com.example.streamingvideo.models;

public class ListLibraries {
    String id_libs;

    public String getId_libs() {
        return id_libs;
    }

    public void setId_libs(String id_libs) {
        this.id_libs = id_libs;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getImplement() {
        return implement;
    }

    public void setImplement(String implement) {
        this.implement = implement;
    }

    String name;
    String link;
    String implement;
}
