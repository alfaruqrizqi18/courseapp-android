package com.example.streamingvideo.models;

public class Video {
    String id_video, title, description, video_url;

    public boolean isIs_played() {
        return is_played;
    }

    public void setIs_played(boolean is_played) {
        this.is_played = is_played;
    }

    boolean is_played;
    int views;

    public String getId_video() {
        return id_video;
    }

    public void setId_video(String id_video) {
        this.id_video = id_video;
    }

    
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVideo_url() {
        return video_url;
    }

    public void setVideo_url(String video_url) {
        this.video_url = video_url;
    }

    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }

}
