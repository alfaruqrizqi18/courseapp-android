package com.example.streamingvideo.multilanguage_helper;

import android.content.Context;
import android.content.res.Configuration;

import java.util.Locale;

public class Language {
    public void setLanguangeRecreate(Context context, String lang) {
        String languageToLoad;
        languageToLoad = lang;
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        context.getResources().updateConfiguration(config,
                context.getResources().getDisplayMetrics());
    }
}
