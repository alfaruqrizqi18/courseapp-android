package com.example.streamingvideo.sharedpreference;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.example.streamingvideo.R;
import com.example.streamingvideo.authactivity.LoginActivity;
import com.example.streamingvideo.models.User;

import java.util.HashMap;
import java.util.Map;

public class SharefPreferenceManager {
    public static final String SHARED_PREF_NAME = "course-app-shared-preference";
    private static final String ID_USER = "id_user";
    private static final String FIRST_NAME = "first_name";
    private static final String LAST_NAME = "last_name";
    private static final String EMAIL = "email";
    private static final String USERNAME = "username";
    private static final String LANGUAGE = "language";
    private static final String CREATED_AT = "created_at";
    private static SharefPreferenceManager mInstance;
    private static Context mCtx;
    SharedPreferences sharedPreferences;

    private SharefPreferenceManager(Context context) {
        mCtx = context;
    }

    public static synchronized SharefPreferenceManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SharefPreferenceManager(context);
        }
        return mInstance;
    }

    public void onLogin(User user) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(ID_USER, user.getId_user());
        editor.putString(FIRST_NAME, user.getFirst_name());
        editor.putString(LAST_NAME, user.getLast_name());
        editor.putString(EMAIL, user.getEmail());
        editor.putString(USERNAME, user.getUsername());
        editor.putString(LANGUAGE, user.getLanguage());
        editor.putString(CREATED_AT, user.getCreated_at());
        editor.apply();
    }

    public User getUser(User user) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return new User(
                sharedPreferences.getString(ID_USER, null),
                sharedPreferences.getString(FIRST_NAME, null),
                sharedPreferences.getString(LAST_NAME, null),
                sharedPreferences.getString(EMAIL, null),
                sharedPreferences.getString(USERNAME, null),
                sharedPreferences.getString(LANGUAGE, null),
                sharedPreferences.getString(CREATED_AT, null)
        );
    }

    public boolean isLoggedIn() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(ID_USER, null) != null;
    }

    public Map<String, String> showSession(){
        sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        Map<String, String> map = new HashMap<String, String>();
        map.put("ID_USER", sharedPreferences.getString(ID_USER,null));
        map.put("FIRST_NAME", sharedPreferences.getString(FIRST_NAME,null));
        map.put("LAST_NAME", sharedPreferences.getString(LAST_NAME,null));
        map.put("EMAIL", sharedPreferences.getString(EMAIL,null));
        map.put("USERNAME", sharedPreferences.getString(USERNAME,null));
        map.put("LANGUAGE", sharedPreferences.getString(LANGUAGE,null));
        map.put("CREATED_AT", sharedPreferences.getString(CREATED_AT,null));
        return map;
    }

    public void logout() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();
        User user = new User (
                null,null,null,null,null,
                mCtx.getResources().getString(R.string.default_lang),null
        );
        onLogin(user);
        Intent intent = new Intent(mCtx, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        mCtx.startActivity(intent);
    }




}
