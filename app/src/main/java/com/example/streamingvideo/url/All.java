package com.example.streamingvideo.url;

public class All {
    public static final String base_url = BaseUrl.base_url_web+"api/";
    public static final String index_course = base_url+"course";
    public static final String view_course = base_url+"course/view/";
    public static final String latest_course = base_url+"course/latest-course";
    public static final String recommended_course = base_url+"course/recommended-course/";

    public static final String enroll_course = base_url+"course/enroll";
    public static final String check_enroll_course = base_url+"course/enroll/check/";

    public static final String finish_course = base_url+"course/finish";


    public static final String index_ongoing_course = base_url+"course/list/ongoing/";
    public static final String index_finish_course = base_url+"course/list/completed/";
    public static final String index_favorite_course = base_url+"course/list/favorite/";
    public static final String index_category = base_url+"category/";
    public static final String index_user_preview = base_url+"user/preview/";
    public static final String index_list_libraries = base_url+"list-libraries";

    public static final String favorite_course = base_url+"course/favorite";
    public static final String delete_favorite_course = base_url+"course/favorite/delete";
    public static final String check_favorite_course = base_url+"course/favorite/check/";

    public static final String add_count_views = base_url+"course/video/views/add";
    public static final String all_video_by_course = base_url+"course/video/list-by-id-course/";


    public static final String save_general_information = base_url+"user/save/general";
    public static final String save_password_changes = base_url+"user/save/password-changes";
    public static final String save_language_changes = base_url+"user/save/language-changes";


}
