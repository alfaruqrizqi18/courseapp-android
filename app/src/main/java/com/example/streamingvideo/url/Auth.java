package com.example.streamingvideo.url;

public class Auth {
    public static final String base_url = BaseUrl.base_url_web+"api/auth/";
    public static final String login = base_url+"login";
    public static final String register = base_url+"register";
}
